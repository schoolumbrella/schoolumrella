@extends('index')
@section('content')
	  <section class="title">
	    <div class="container">
	      <div class="row-fluid">
	        <div class="span6">
	          <h1>Student</h1>
	        </div>
	        <div class="span6">
	          <ul class="breadcrumb pull-right">
	            <li><a href="{{url()}}">Home</a> <span class="divider">/</span></li>
	            <li class="active">Student</li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </section>
	  <!-- / .title -->       


<!-- yo laravel style ko form ho -->
{!!Form::open(array('url'=>'studentSearch','method'=>'post','class'=>'center','enctype'=>'multipart/form-data'))!!}
{!!Form::label('affiliateId', 'University:')!!}
{!!Form::select('affiliateId', $affiliates, '1', array('onChange'=>'affiliateChange()'))!!}


{!!Form::label('courseId', 'Course:')!!}
{!!Form::select('courseId',array(),'')!!}
{!!Form::close()!!}




	  <section  class="container">
	      <table>
	      	<tr>
	      		<td>S.N.</td>
	      		<td>Description</td>
	      		<td>File</td>
	      	</tr>
	      	@foreach($resources as $index => $resource)
	    	  	<tr>
	    	  		<td>{{$index+1}}</td>
	    	  		<td>{{$resource->description}}</td>
	    	  		@foreach($resource->resource_files as $resource_file)
	    	  			<td><a href="{{url().'/uploads/resources/'.$resource_file['file']}}">{{$resource_file['name']}}</a></td><!-- $resource_file is function name in Resource Model i.e. ORM -->
	    	  		@endforeach
	    	  		
	    	  	</tr>	
	      	@endforeach
	      </table>
	  </section>
	
@endsection