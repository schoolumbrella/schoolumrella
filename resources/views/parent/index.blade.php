@extends('index')
@section('content')


	  <section class="title">
	    <div class="container">
	      <div class="row-fluid">
	        <div class="span6">
	          <h1>Parent</h1>
	        </div>
	        <div class="span6">
	          <ul class="breadcrumb pull-right">
	            <li><a href="{{url()}}">Home</a> <span class="divider">/</span></li>
	            <li class="active">Parent</li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </section>

	  <div>
	  	{!!Form::open(array('url'=>"/parent",'method' => 'post'))!!}
		  	{!!Form::label('affiliates','Affiliates')!!}
		  	{!!Form::select('affiliateId',$affiliates,null, array('onchange' => 'getCourses()','id' => 'affiliateId'))!!}
		  	{!!Form::label('streams','Streams')!!}
		  	{!!Form::select('streamId',$streams,null,array('onchange' => 'getCourses()','id' =>'streamId'))!!}
		  	{!!Form::label('courses','Courses')!!}
		  	{!!Form::select('courseId',$courses,null,array('onchange' => 'getOrganizationsByCourseId()','id'=> 'courseId'))!!}
		  	{!!Form::label('organizations','Organization')!!}
		  	{!!Form::select('organizationId',$organizations,null,array('id' => 'organizationId','onchange'=>'selectOrg()'))!!}
		  	{!!Form::label('search','Search')!!}
		  	{!!Form::input('search',null,null,array('name' => 'search'))!!}
		  	{!!Form::submit('Submit')!!}
	  	{!!Form::close()!!}
	  </div>

	  @if (count($errors) > 0)
	      <div class="alert alert-danger">
	          <ul>
	              @foreach ($errors->all() as $error)
	                  <li>{{ $error }}</li>
	              @endforeach
	          </ul>
	      </div>
	  @endif
	  
	  <section id="registration-page" class="container">
	  	<table id="orgTable">
	  		<thead>
	  			<tr>
	  				<td>Name</td>
	  				<td>Adress</td>
	  				<td>description</td>
	  				<td>brochure</td>
	  			</tr>
	  		</thead>
	  		<tbody>
	  			@foreach($org_profiles as $org_profile)
	  				<tr id = '{{$org_profile->id}}'>
	  					<td>{{$org_profile->name}}</td>
	  					<td>{{$org_profile->address}}</td>
	  					<td>{{$org_profile->description}}</td>
	  					<td><a href="{{url().'/uploads/'.$org_profile->brochure}}">Brochure{{$org_profile->brochure}}</a></td>
	  					<td><button data-toggle="modal" data-myid="{{$org_profile->userId}}" data-target="#commentModal">Comment</button></td>
	  					<td>
	  						@foreach($org_profile->user->comments as $comment)
	  						 <span>{{$comment->comment}}</span>
	  						 <span>By:{{$comment->user->username}}</span>
	  						 	@foreach($comment->comments() as $cmnt)
	  						 	<span>{{$cmnt->comment}}</span>
	  						 	@endforeach
	  						@endforeach
	  					</td>
	  				 	
	  					<td>
	  					<table>
	  						@foreach($org_profile->courses as $course)
	  								<tr>
	  									<td>{{$course->name}}</td>
	  									<td>{{$course->affiliate->name}}</td>
	  									<td>{{$course->stream->name}}</td>
	  									<td>{{$course->level->name}}</td>
	  								</tr>
	  						@endforeach
	  					</table>
	  					
	  					</td>
	  				</tr>
	  			@endforeach
	  		</tbody>
	  		
	  	</table>
	    
	  </section>

	    <div id="commentModal" class="modal fade" role="dialog">
	      <div class="modal-dialog">
	        <!-- Modal content-->
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	            <h4 class="modal-title">Post  Comments</h4>
	          </div>
	          <div class="modal-body">
	          {!!Form::open(array('url'=>"/comment",'method' => 'post'))!!}
	          	<input type="hidden" name="comment_typeId" value="{{$comment_type->id}}">
	          	<input type="hidden" name="organizationId" id="organizationId">
	          	<textarea name="comment"></textarea>
	            <button>COMMENT</button>
	           {!!Form::close()!!} 
	          </div>
	          <div class="modal-footer">
	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	          </div>
	        </div>
	      </div>
	    </div>
	  
@endsection