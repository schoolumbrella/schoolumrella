              	{!!Form::open(array('url' => "teacher/resourceEdit/{$resource->id}",'method' => 'post', 'enctype' => 'multipart/form-data'))!!}
              	
              	<div class="controls">
              	<label>Resource Type</label>
              	  <select  name="resource_typeId" class="input-xlarge" autofocus> 
              	  	<option>Select Type</option>
              	  	@foreach($resourceTypes as $resourceType)
              	  		<option value="{{$resourceType->id}}" <?php if($resourceType->id == $resource->resource_typeId){echo 'selected'; } ?> >{{$resourceType->name}}</option>
              	  	@endforeach
              	  </select> 
              	</div>
              	<div class="controls">
              	<label>Description</label>
              	  <textarea name="description" placeholder="Description">{{$resource->description}}</textarea> 
              	</div>
              	<div class="controls">
              	<label>Targeted Courses</label>
              		<?php 
              			$affiliate = null;
              			$stream = null;
              			$level = null;
              		?>
              	  @foreach($courses as $course)
              	  	<?php
              	  		if($affiliate  != $course->affiliate['name']){
              	  			echo $course->affiliate['name']."<br>";
              	  		}
              	  		if($affiliate  != $course->affiliate['name'] || $stream != $course->stream['name']){
              	  			echo $course->stream['name']."<br>";
              	  		}
              	  		if($affiliate  != $course->affiliate['name'] || $stream != $course->stream['name'] || $level != $course->level['name']){
              	  			echo $course->level['name'];
              	  		}
              	  		echo "<br> &nbsp".$course->name;
              	  		$affiliate = $course->affiliate['name'];
              	  		$stream = $course->stream['name'];
              	  		$level = $course->level['name'];
              	  		?>
              	  		<div class="checkbox">
              	  		<?php
              	  		if($course->courseType == 's'){
              	  			for($i = 0; $i < $course->duration; $i++){
              	  				?>
              	  				
              	  				  <label>
              	  				  <input name="semesters[{{$course->id}}][]" type="radio" value="{{$i+1}}"
              	  				  @foreach($resource->resource_courses as $resource_course)
	              	  				  <?php 
	              	  				  if($course->id == $resource_course['courseId'] && $i+1 == $resource_course['semesters']){echo 'checked'; }
	              	  				  ?>
              	  				  @endforeach
              	  				  >{{$i+1}} Semester</label>

              	  				<?php
              	  			}
              	  		}
              	  		else{
              	  			for($i = 0; $i < $course->duration; $i++){
              	  				?>
              	  				  <label>
              	  				  <input name="semesters[]" type="radio" value="{{$i+1}}">{{$i+1}} Year</label>

              	  				<?php
              	  			}
              	  		}
              	  	 ?>
              	  	</div>
              	  @endforeach 
              	</div>
              	@foreach($resource->resource_files as $resource_file)
              		<div class="controls">
              			<label>Name</label>
              			<input type="text" name="name" class="input-xlarge" value="{{$resource_file['name']}}">
              		</div>
              		<div class="controls">
              			<label>File</label>
              			<input type="file" name="file" class="input-xlarge" >{{$resource_file['file']}}
              		</div>
              		<div class="controls">
              			<label>Order</label>
              			<input type="number" name="order" class="input-xlarge" value="{{$resource_file['order']}}">
              		</div>
              	@endforeach
    	          	
              	</div>
              	<button>Update</button> 
              	{!!Form::close()!!}