@extends('index')
@section('content')

	@if(session('sucess_message'))
        {{session('sucess_message')}}
    @endif

    @if(session('error_message'))
        {{session('error_message')}}
    @endif


            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Resource</h4>
              </div>
              <div class="modal-body">
              	{!!Form::open(array('url' => 'teacher/resourceAdd','method' => 'post', 'enctype' => 'multipart/form-data'))!!}
              	
              	<div class="controls">
              	<label>Resource Type</label>
              	  <select  name="resource_typeId" class="input-xlarge"> 
              	  	<option>Select Type</option>
              	  	@foreach($resourceTypes as $resourceType)
              	  		<option value="{{$resourceType->id}}">{{$resourceType->name}}</option>
              	  	@endforeach
              	  </select> 
              	</div>
              	<div class="controls">
              	<label>Description</label>
              	  <textarea name="description" placeholder="Description"></textarea> 
              	</div>
              	<div class="controls">
              	<label>Targeted Courses</label>
              		<?php 
              			$affiliate = null;
              			$stream = null;
              			$level = null;
              		?>
              	  @foreach($courses as $course)
              	  	<?php
              	  		if($affiliate  != $course->affiliate['name']){
              	  			echo $course->affiliate['name']."<br>";
              	  		}
              	  		if($affiliate  != $course->affiliate['name'] || $stream != $course->stream['name']){
              	  			echo $course->stream['name']."<br>";
              	  		}
              	  		if($affiliate  != $course->affiliate['name'] || $stream != $course->stream['name'] || $level != $course->level['name']){
              	  			echo $course->level['name'];
              	  		}
              	  		echo "<br> &nbsp".$course->name;
              	  		$affiliate = $course->affiliate['name'];
              	  		$stream = $course->stream['name'];
              	  		$level = $course->level['name'];
              	  		?>
              	  		<div class="checkbox">
              	  		<?php
              	  		if($course->courseType == 's'){
              	  			for($i = 0; $i < $course->duration; $i++){
              	  				?>
              	  				
              	  				  <label>
              	  				  <input name="semesters[{{$course->id}}][]" type="radio" value="{{$i+1}}">{{$i+1}} Semester</label>

              	  				<?php
              	  			}
              	  		}
              	  		else{
              	  			for($i = 0; $i < $course->duration; $i++){
              	  				?>
              	  				  <label>
              	  				  <input name="semesters[]" type="radio" value="{{$i+1}}">{{$i+1}} Year</label>

              	  				<?php
              	  			}
              	  		}
              	  	 ?>
              	  	</div>
              	  @endforeach 
              	</div>
    	          	<div class="controls">
    	          		<label>Name</label>
    	          		<input type="text" name="name" class="input-xlarge">
    	          	</div>
    	          	<div class="controls">
    	          		<label>File</label>
    	          		<input type="file" name="file" class="input-xlarge">
    	          	</div>
    	          	<div class="controls">
    	          		<label>Order</label>
    	          		<input type="number" name="order" class="input-xlarge">
    	          	</div>
              	</div>
              	<button>Add</button> 
              	{!!Form::close()!!}
              </div>
            </div>

@endsection