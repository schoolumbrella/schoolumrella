@extends('index')
@section('content')

	@if(session('sucess_message'))
        {{session('sucess_message')}}
    @endif

    @if(session('error_message'))
        {{session('error_message')}}
    @endif

	  <section class="title">
	    <div class="container">
	      <div class="row-fluid">
	        <div class="span6">
	          <h1>Teacher</h1>
	        </div>
	        <div class="span6">
	          <ul class="breadcrumb pull-right">
	            <li><a href="{{url()}}">Home</a> <span class="divider">/</span></li>
	            <li class="active">Teacher</li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </section>
	  <!-- / .title -->       

	  <table>
	  	<tr>
	  		<td>S.N.</td>
	  		<td>Description</td>
	  	</tr>
	  	@foreach($resources as $index => $resource)
		  	<tr>
		  		<td>{{$index+1}}</td>
		  		<td>{{$resource->description}}</td>

		  		<td><a href='{{url("teacher/resourceDel/$resource->id")}}'>Del</a></td>
		  		<td><a href='{{url("teacher/resourceEdit/$resource->id")}}'>Edit</a></td>
		  	</tr>	
	  	@endforeach
	  </table>
	  
	  <a href="{{url('teacher/resourceAdd')}}">Add Resource</a>

@endsection