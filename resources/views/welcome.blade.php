@extends('index')
@section('content')
    @if(session('sucess_message'))
        {{session('sucess_message')}}
    @endif

    @if(session('error_message'))
        {{session('error_message')}}
    @endif
    @if(count($errors)>0)
        <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach    
        </ul>
        </div>
    @endif
       <!-- banner -->
       <div class="banner">
        <div class="container">
            <script src="js/responsiveslides.min.js"></script>
                <script>
                    // You can also use "$(window).load(function() {"
                    $(function () {
                     // Slideshow 4
                    $("#slider3").responsiveSlides({
                        auto: true,
                        pager: true,
                        nav: false,
                        speed: 500,
                        namespace: "callbacks",
                        before: function () {
                    $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                        $('.events').append("<li>after event fired.</li>");
                        }
                        });
                        });
                </script>

                <div  id="top" class="callbacks_container">
                    <ul class="rslides" id="slider3">
                        <li>
                            <div class="banner-info">
                                <h3>Make A Huge Difference Start Your Career journey With Us</h3>
                            </div>
                        </li>
                        <li>
                            <div class="banner-info">
                                <h3>Learning Online Becomes Easier And Faster</h3>
                            </div>
                        </li>
                        <li>
                            <div class="banner-info">
                                <h3>Make A Huge Difference Start Your Career journey With Us</h3>
                            </div>
                        </li>
                        <li>
                            <div class="banner-info">
                                <h3>Learning Online Becomes Easier And Faster</h3>
                            </div>
                        </li>
                    </ul>
                </div>
        </div>
       </div>
       <!-- //banner -->
@endsection
