<!DOCTYPE html>
<html>
<head>
<title>SchoolUmbrella</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tutelage Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
{!!Html::style('css/bootstrap.css')!!}
{!!Html::style('css/style.css')!!}
{!!Html::style('css/swipebox.css')!!}
<!-- js -->
{!!Html::script('js/jquery-1.11.1.min.js')!!}
{!!Html::script('js/modernizr.custom.js')!!}
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Alegreya:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- //fonts -->
    <!-- start-smoth-scrolling -->
        {!!Html::script('js/move-top.js')!!}
        {!!Html::script('js/easing.js')!!}
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
    <!-- start-smoth-scrolling -->
    <style type="text/css">
            #organizationInfo{
                display: none;
            }
        </style>

</head>

    <body>
        <!-- header -->
        <div class="header">
                <div class="container">
                    <div class="header-nav">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <h1><a class="navbar-brand" href="{{url()}}">
                                    <i class="glyphicon glyphicon-education" aria-hidden="true"></i>
        <span>School</span>Umbrella</a></h1>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a class="hvr-overline-from-center button2 active" href="{{url()}}">Home</a></li>
                                    @if(Auth::check())
                                        <li><a class="hvr-overline-from-center button2" href="{{url('auth/logout')}}">Log Out</a></li>
                                        @else
                                         <li><a class="hvr-overline-from-center button2" href="{{url('login')}}">Login</a></li>
                                        <li><a class="hvr-overline-from-center button2" href="{{url('register')}}">Sign Up</a></li>
                                       
                                    @endif
                                </ul>
                            </div><!-- /navbar-collapse -->
                            
                            <!-- search-scripts -->
                            <script src="js/classie.js"></script>
                            <script src="js/uisearch.js"></script>
                                <script>
                                    new UISearch( document.getElementById( 'sb-search' ) );
                                </script>
                            <!-- //search-scripts -->
                        </nav>
                    </div>
                </div>
        </div>
        <!-- header -->
        @yield('content')

        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-grids">
                    <div class="col-md-3 footer-grid">
                        <h3>ADDRESS</h3>
                        <ul>

                            <li><a href="#"> Baluwatar (Near Tundaldevi Temple), Kathmandu, Nepal</a></li>
                            <li><a data-toggle="modal" data-target="#contactModal">info@schoolumbrella.com</a></li>
                            <li><a href="#">www.schoolumbrella.com</a></li>
                            <li><a href="#"> +977-1-401000</a></li>
                            
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h3>PRAESENTIUM </h3>
                        <ul>
                            <li><a data-toggle="modal" data-target="#subscribeModal">Subscribe</a></li>
                            <li><a href="#">SEMINARS</a></li>
                            <li><a href="#">PREPARATIONS</a></li>
                            <li><a href="#">CONDUCTING GAMES</a></li>
                            <li><a href="#">OTHER ACTIVITIES</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h3>DIGNISSIMOS</h3>
                        <ul>
                            <li><a href="#">SUMMER CAMPS</a></li>
                            <li><a href="#">CELEBRATIONS</a></li>
                            <li><a href="#">ONLINE EXAMS</a></li>
                            <li><a href="#">COMPETITIONS</a></li>
                            <li><a href="#">ACTIVITIES</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h3>PRAESENTIUM</h3>
                        <ul>
                            <li><a href="#">PRESENTATIONS</a></li>
                            <li><a href="#">SEMINARS</a></li>
                            <li><a href="#">PREPARATIONS</a></li>
                            <li><a href="#">CONDUCTING GAMES</a></li>
                            <li><a href="#">OTHER ACTIVITIES</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p> &copy; 2016 SchoolUmbrella. All Rights Reserved | Design by  <a href="http://w3layouts.com/"></a><a href="#">SchoolUmbrella IT Team</a></p>
            </div>
        </div>
        <!-- //footer -->
    <!--  Login form -->
    <div class="modal hide fade in" id="loginForm" aria-hidden="false">
        <div class="modal-header">
            <i class="icon-remove" data-dismiss="modal" aria-hidden="true"></i>
            <h4>Login Form</h4>
        </div>
        <!--Modal Body-->
        <div class="modal-body">
            <form class="form-inline" action="index.html" method="post" id="form-login">
                <input type="text" class="input-small" placeholder="Email">
                <input type="password" class="input-small" placeholder="Password">
                <label class="checkbox">
                    <input type="checkbox"> Remember me
                </label>
                <button type="submit" class="btn btn-primary">Sign in</button>
            </form>
            <a href="#">Forgot your password?</a>
        </div>
        <!--/Modal Body-->
    </div>
    <!--Contact  Modal -->
     <div id="contactModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Update Courses</h4>
            </div>
            <div class="modal-body">
            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>

      <!--Subscribe  Modal -->
       <div id="subscribeModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Subscribe</h4>
              </div>
              <div class="modal-body">
                {!!Form::open(array('url'=>url('subscribe'),'method'=>'post'))!!}
                {!!Form::label('email','Email')!!}
                {!!Form::email('email',null,array())!!}
                {!!Form::submit('submit')!!}
                {!!Form::close()!!}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

    <!-- for bootstrap working -->
    {!!Html::script('js/bootstrap.js')!!}
    {!!Html::script('js/custom.js')!!}
    <!-- //for bootstrap working -->
    <!-- smooth scrolling -->
        <script type="text/javascript">
            $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            */                              
            $().UItoTop({ easingType: 'easeOutQuart' });
            });
        </script>
        <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <!-- //smooth scrolling -->


    </body>
</html>
