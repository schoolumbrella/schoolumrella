@extends('index')
@section('content')
<!-- banner -->
<div class="banner page-head">
</div>
<!-- //banner -->
<div class="typrography">
	 <div class="container">
		<h2 class="tittle">Register</h2>

	  <section id="registration-page" class="container">
	    <!-- <form class="center" action='' method="POST"> -->
	    {!!Form::open(array('url'=>'register','method'=>'post','class'=>'center','enctype'=>'multipart/form-data'))!!}
	      <fieldset class="registration-form col-sm-offset-3 col-sm-6">
	
	      	<div class="input-group">
	          <!-- Member Type-->
	          <label class="input-group-addon">User Type</label>
	            <select onchange="changeUserType()" id="user_typeId" name="user_typeId" class="form-control"> 
	            	<option value="2">Student</option>
	            	<option value="3">Parent</option>
	            	<option value="4">Teacher</option>
	            	<option value="5">Organization</option>
	            </select>
	        </div>
	       
	        <div id="organizationInfo">
	        	<div class="input-group">
	        	    <!-- Firstname -->
	        	   <label class="input-group-addon">Name</label>
	        	    <input type="text"  value="" name="name" placeholder="Name" class="form-control">
	        	   
	        	   
	        	 </div>
	        	 <div class="input-group">
	        	   <label class="input-group-addon">Address</label>
	        	    <input type="text" name="address" placeholder="Address" class="form-control">
	        	   
	        	   
	        	 </div>
	        	  <div class="control-group">
					<label class="">Description</label>
	        	    <div class="controls">
	        	      	<textarea  rows="10" cols="20" name="description" placeholder="Description" class="input-xlarge">
	        	      	
	        	    	</textarea>
	        	    </div>
	        	  </div>
				<div class="input-group">
	        	   <label class="input-group-addon">File</label>
	        	    <!-- <input type="text" name="address" placeholder="Address" class="form-control"> -->
	        	   <input type="file" name="brochure" class="form-control">
	        	 </div>
	        	 
	        </div>
	      	<div id="extraInfo">
	      		  <div class="input-group">
	      		    <!-- Firstname -->
	      		    <level class="input-group-addon"> First Name</level>
	      		      <input type="text"  name="fname" placeholder="Firstname" class="form-control">
	      		    
	      		  </div>

	      		  <div class="input-group">
	      		    <!-- Middlename -->
	      		    <level class="input-group-addon"> Middle Name</level>
	      		      <input type="text"  name="mname" placeholder="Middlename" class="form-control">
	      		    
	      		  </div>
	      		  <div class="input-group">
	      		    <!-- Middlename -->
	      		    <level class="input-group-addon"> Last Name</level>
	      		      <input type="text"  name="lname" placeholder="Lastname" class="form-control">
	      		    
	      		  </div>
	      		  

	      		  <div class="input-group">
	      		    <!-- Gender -->
	      		    <level class="input-group">Gender</level>
	      		    <div class="controls">
	      		      <label> <input type="radio" value="f" name="gender" class="input-xlarge">Female </label>
	      		      <label> <input type="radio" value="m" name="gender" class="input-xlarge">Male </label>
	      		      <label> <input type="radio" value="o" name="gender" class="input-xlarge">Others </label>
	      		    </div>
	      		  </div>
	      	</div>
	      	
	        <div class="control-group">
	          <div class="input-group">
	      		    <level class="input-group-addon">User Name</level>
	      		      <input type="text"  name="username" placeholder="Username" class="form-control">
	      		</div>
	        </div>
	        <div class="control-group">
	          <div class="input-group">
	      		    <level class="input-group-addon">Email</level>
	      		      <input type="email"  name="email" placeholder="Email" class="form-control">
	      		</div>
	        </div>
	        <div class="control-group">
	          <div class="input-group">
	      		    <level class="input-group-addon">Password</level>
	      		      <input type="password"  name="password" placeholder="Password" class="form-control">
	      		</div>
	        </div>
	        <div class="control-group">
	          <div class="input-group">
	      		    <level class="input-group-addon">Password (Confirm)</level>
	      		      <input type="password"  name="password_confirm" placeholder="Password (Confirm)" class="form-control">
	      		</div>
	        </div>
	        <div class="control-group">
	          <!-- Button -->
	          <div class="controls">
	            <button class="btn btn-success btn-large btn-block">Register</button>
	          </div>
	        </div>
	      </fieldset>
	    <!-- </form> -->

	    {!!Form::close()!!}
	  </section>
	  	</div>
	  </div>
	        
	  <!-- /#registration-page -->
@endsection