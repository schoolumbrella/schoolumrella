@extends('index')
@section('content')
<div class="features">
  <div class="container">
    <h3 class="tittle">
    Organization
    </h3> 
    <div class="col-md-5 features-left">
      <img src="images/f1.jpg" alt=""/>
    </div>
    <div class="col-md-7 features-right">
      <h4>
        @if($organization)
        {{$organization->name}}
        @else
          Organization name is not added !!
        @endif
      </h4>
        <p>
          @if($organization)
        {{$organization->description}}
        @endif
        </p>

    </div>
    <div class="row-fluid">
        <div class="span6">
            <h3>Address</h3>
            @if($organization)
            <p>{{$organization->address}}</p>
            @endif
        </div>
    </div>
    <div class="row-fluid">
      <div class="span9">
          <!-- This div is used for empty space -->
      </div>
      <div class="span3">
          <a class="btn btn-success btn-large pull-right" href="{{url('organization/edit')}}">Edit Profile</a>
      </div>
    </div>
    <div class="row-fluid">
           
            <div class="span6">
                <h2>Our Courses</h2>
                <div>
                <ol>
                  @foreach($organization->courses as $course)
                    
                      <li>
                        {{$course->name}}
                        <ul>
                          <li>{{$course->affiliate->name}}</li>
                          <li>{{$course->stream->name}}</li>
                          <li>{{$course->level->name}}</li>
                        </ul>
                      </li>  
                    
                  @endforeach
                  </ol>
                </div>
            </div>
        </div>
    <div class="clearfix"></div>
  </div>
</div>
        <div class="row-fluid">
            <div class="span6">
                <h3>Address</h3>
                @if($organization)
                <p>{{$organization->address}}</p>
                @endif
            </div>
        </div>
        @foreach($organization->user->comments as $comment)
        <span>{{$comment->comment}}</span>
        <span>By:{{$comment->user->username}}</span>
        <button data-toggle="modal" data-myid="{{$comment->id}}" data-target="#commentModal">Comment</button>
          @foreach($comment->comments() as $cmnt)
          <span>{{$cmnt->comment}}</span>
          @endforeach
        @endforeach
        <div id="commentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Post  Comments</h4>
            </div>
            <div class="modal-body">
            {!!Form::open(array('url'=>"/organization/comment",'method' => 'post'))!!}
        
              <input type="hidden" name="organizationId" id="organizationId">
              <textarea name="comment"></textarea>
              <button>COMMENT</button>
             {!!Form::close()!!} 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
@endsection