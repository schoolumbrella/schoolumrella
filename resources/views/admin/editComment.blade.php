@extends('admin.index')
@section('content')

	 {!!Form::open(array('url' => url('myAdmin/comment/edit/'.$comment->id), 'method' =>'post'))!!}
	 {!!Form::label('status','Status')!!}
	 {!!Form::select('status', array('0'=>'unpublished', '1'=>'published'),$comment->published)!!}
	 {!!Form::label('comment','Comment')!!}
	 {!!Form::textarea('comment',$comment->comment,['size'=>'30x5'])!!}
	 
	 {!!Form::label('user','User')!!}
	 <span>{{$comment->user->username}}</span>
	 
	 {!!Form::label('commentFor','CommentFor')!!}
	 <span>{{$comment->comment_type->name}}</span>

	 {!!Form::label('commentRoot','CommentRoot')!!}
	 <span>{{$comment->commentRoot()->username}}</span>
	 
	 {{$comment->user}}
	 {{$comment->comment_type}}
	 {{$comment->commentRoot()}}
	 {!!Form::submit('Update')!!}
	 {!!Form::close()!!}

@endsection