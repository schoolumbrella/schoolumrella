@extends('admin.index')
@section('content')
	
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	
	@if(session('sucess_message'))
		{{session('sucess_message')}}
	@endif

	@if(session('error_message'))
		{{session('error_message')}}
	@endif
	{!!Form::open(array('url' =>"myAdmin/subscribers", 'method' => 'post'))!!}
	{!!Form::label('subject','Subject')!!}
	{!!Form::text('subject')!!}
	{!!Form::label('message','Message')!!}
	{!!Form::textarea('message')!!}
	{!!Form::submit('submit')!!}
	{!!Form::close()!!}
	<table>
		<tr>
			<td>S.N.</td>
			<td>Email</td>
			<td>IP</td>
		</tr>
		@foreach($subrs as $index => $subscriber)
			<tr>
				<td>{{$index+1}}</td>
				<td>{{$subscriber->email}}</td>
				<td>{{$subscriber->ip}}</td>
			</tr>
		@endforeach	
	</table>

@endsection