@extends('admin.index')
@section('content')
	<table>
		<tr>
			<td>S.N.</td>
			<td>Comment</td>
			<td>Action</td>
		</tr>
		@foreach($comments as $index => $comment)
			<tr>
				<td>{{$index+1}}</td>
				<td>{{$comment->comment}}</td>
				<td><a href='{{url("/myAdmin/comment/edit/$comment->id")}}'>Edit</a></td>
				<td><a href='{{url("/myAdmin/comment/delete/$comment->id")}}'>Delete</a></td>
			</tr>

		@endforeach

	</table>
@endsection