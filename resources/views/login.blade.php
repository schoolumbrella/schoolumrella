@extends('index')
@section('content')
	<!-- banner -->
	<div class="banner page-head">
	</div>
	<!-- //banner -->
	<div class="typrography">
		 <div class="container">
			<h2 class="tittle">Login</h2>
			<section id="registration-page">
			@if(session('error_message'))
				{{session('error_message')}}
			@endif
			  <!-- <form class="center" action='' method="POST"> -->
			  {!!Form::open(array('url'=>'login','method'=>'post','class'=>'center'))!!}
			    <fieldset class="registration-form col-sm-offset-3 col-sm-6">
					<div class="input-group">
					  <level class="input-group-addon" id="basic-addon1"></level>
					  <input name="username" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
					</div>
					<div class="input-group">
					  <level class="input-group-addon" id="basic-addon1"></level>
					  <input name="password" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
					</div>
			      <div class="control-group">
			        <!-- Button -->
			        <div class="controls">
			          <button class="btn btn-success btn-large btn-block">Login</button>
			        </div>
			      </div>

			      <div class="control-group">
			        <!-- Button -->
			        <div class="controls">
			          <a href="{{url('register')}}" class="btn btn-success btn-large btn-block">Register</a>
			        </div>
			      </div>

			    </fieldset>
			  <!-- </form> -->
			  {!!Form::close()!!}
			</section>
			<!-- /#registration-page -->
			
		</div>
	</div>
       


	  
@endsection