<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Comment;
use App\Subscriber;
use App\Subscriber_message;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // // $admin = Admin::where('userId',Auth::user()->id)->first();
        // // return view('admin')->with('admin',$admin);
        // return view('admin::login');

        if(Auth::check() && Auth::user()->user_typeId == 1) {
            return redirect('myAdmin');
        }
        else {
            return view('admin.login');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function login(Request $request) 
    {
        //return $request['username'];
    if(Auth::attempt(['username'=>$request['username'], 'password'=>$request['password']]) && Auth::user()->user_typeId == 1)
        return redirect('myAdmin');  
    else 
        {
            \Session::flash('error_message','Invalid credentials!');
            return redirect('');
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function home() {

            $admin = User::find(Auth::user()->id);
            return view('admin.home')->with('admin',$admin); 

    }
    public function comment(){
        $comments = Comment::where('published',0)->get();
        return view('admin.comment')->with('comments',$comments);
    }
    public function editComment($id){
        $comment = Comment::find($id);
        return view('admin.editComment')->with('comment',$comment);
    }
    public function updateComment(Request $request, $id){
        $comment = Comment::find($id);
        $comment->published = $request['status'];
        $comment->save();
        \Session::flash('sucess_message','Sucessfully Updated');
        return redirect('myAdmin/comments');
    }
    public function deleteComment($id){
        $comment = Comment::find($id);
        $comment->delete();
        \Session::flash('sucess_message','Deleted sucessfully');
        return redirect('myAdmin/comments');
    }

    public function subscriber(){
        //$subs = Subscriber::all();
        return view('admin.subscribers')->with('subrs', Subscriber::all());
    }
    public function message(Request $request){
        $request['userId'] = Auth::user()->id;
        $this->validate($request,Subscriber_message::$rules);
        Subscriber_message::create($request->all());
        // multiple recipients
        $subs = Subscriber::select('email')->get();
        $to = '';
        foreach ($subs as  $index => $subscriber) {
            if($index == 0){
                $to .= $subscriber->email;
            }
            else{
               $to .= ','.$subscriber->email; 
            }
            
        }
        // subject
        $subject = $request['subject'];

        // message
        $message = "
        <html>
        <style>
        </style>
        <head>
          <title>School Umbrella</title>
        </head>
        <body>
          <p>".$request['message']."</p>
        </body>
        </html>
        ";

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        // $headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
        // $headers .= 'From: Birthday Reminder <birthday@example.com>' . "\r\n";
        // $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
        // $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

        // Mail it
        mail($to, $subject, $message, $headers);
        \Session::flash('sucess_message','Info is Sucessfully Sent');
        return redirect('myAdmin/subscribers');
    }
}
