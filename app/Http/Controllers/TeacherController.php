<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User,App\Resource_type,App\Course;
use App\Resource;
use App\Resource_course;
use App\Resource_file;


class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher = User::find(Auth::user()->id);
        $resourceTypes = Resource_type::all();
        $courses = Course::orderBy('affiliateId', 'DESC')->orderBy('streamId','DESC')->orderBy('levelId','DESC')->get();
        $resources = Resource::where('teacherId', Auth::user()->id)->get();
        return view('teacher.index')->with('teacher',$teacher)->with('resourceTypes',$resourceTypes)->with('courses',$courses)->with('resources', $resources);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = User::find(Auth::user()->id);
        $resourceTypes = Resource_type::all();
        $courses = Course::orderBy('affiliateId', 'DESC')->orderBy('streamId','DESC')->orderBy('levelId','DESC')->get();
        return view('teacher/resourceAdd')->with('teacher',$teacher)->with('resourceTypes',$resourceTypes)->with('courses',$courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = new Resource;
        $res->description = $request['description'];
        $res->teacherId = Auth::user()->id;
        $res->resource_typeId = $request['resource_typeId'];
        $res->save();

        foreach ($request['semesters'] as $key => $value) {
            $rescrs = new Resource_course;
            $rescrs->resourceId = $res->id;
            $rescrs->courseId= $key;
            $rescrs->semesters = $value[0];
            $rescrs->save();
        }   

        $resfile = new Resource_file;
        $resfile->name = $request['name'];
        $resfile->order = $request['order'];
            
        $name = time().".".$request->file('file')->getClientOriginalExtension();
        $dest = base_path()."/public/uploads/resources/";
        $request->file('file')->move($dest,$name);
        $resfile->file = $name;
         
        $resfile->resourceId = $res->id;
        $resfile->save();
        \Session::flash('sucess_message','Resource material is sucessfully stored');
        return redirect('teacher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = Resource::find($id);
        $res_types = Resource_type::all();
        $crs = Course::all();
        return view('teacher/resourceEdit')->with('resource',$res)->with('resourceTypes',$res_types)->with('courses',$crs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = Resource::find($id);
        $res->description = $request['description'];
        $res->resource_typeId = $request['resource_typeId'];
        $res->save();
        
        foreach ($res->resource_files as $key => $resource_file) {
                $resfile = Resource_file::find($resource_file['id']);
                $resfile->name = $request['name'];
                $resfile->order = $request['order'];

                if($request['file']){
                  $name = time().".".$request->file('file')->getClientOriginalExtension();
                  $dest = base_path()."/public/uploads/resources/"; //base_path()fun gives project location i.e. root 
                  $request->file('file')->move($dest,$name);

                  $filepath = base_path()."/public/uploads/resources/".$resfile->file;
                  is_file($filepath)? file_exists($filepath)? unlink($filepath): " ": " "; //unlink() function is used to delete file 

                $resfile->file = $name; // all above 3 lines only for this line to save name in the table.
                }
                $resfile->save();
        } 

       foreach ($res->resource_courses as $key => $resource_course) {
            $rescrs = Resource_course::find($resource_course['id']);
            $rescrs->delete();
        }
        
        foreach ($request['semesters'] as $key => $value) {
            $rescrs = new Resource_course;
            $rescrs->resourceId = $res->id;
            $rescrs->courseId= $key;
            $rescrs->semesters = $value[0];
            $rescrs->save();
        }
        \Session::flash('sucess_message','Sucessfully Updated!'); 
        return redirect('teacher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Resource::find($id);
        $res->delete();
        return redirect('teacher');
    }
}
