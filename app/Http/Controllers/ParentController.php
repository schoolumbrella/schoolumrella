<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Organization_profile;
use App\Comment_type;
use App\Affiliate;
use App\Stream;
use App\Course;
use DB;

class ParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parent = User::find(Auth::user()->id);
        $org_profiles = Organization_profile::all();
        $comment_type = Comment_type::where('name','organization')->first();
        $affiliates = Affiliate::lists('name','id'); // for search purpose
        // $streams = Stream::lists('name','id');
        // $courses = Course::lists('name','id');
        // $organizations = Organization_profile::lists('name','id');

        return view('parent.index')->with('parent',$parent)
            ->with('org_profiles',$org_profiles)
            ->with('comment_type',$comment_type)
            ->with('affiliates',$affiliates)
            ->with('streams',Stream::lists('name','id')) 
            ->with('courses', Course::lists('name','id'))
            ->with('organizations', Organization_profile::lists('name','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request){
        $parent = User::find(Auth::user()->id);
        $org_profiles = Organization_profile::all();
        $comment_type = Comment_type::where('name','organization')->first();
        $affiliates = Affiliate::lists('name','id'); // for search purpose
        // $streams = Stream::lists('name','id');
        // $courses = Course::lists('name','id');
        // $organizations = Organization_profile::lists('name','id');

        // $search  = $request['search'];
        // $first = DB::table('organization_profiles')->where('name','LIKE','%'.$request['search'].'%');
        if($request['search']){
            $comments = DB::table('comments')->where('comment','LIKE','%'.$request['search'].'%');

        $results =  Organization_profile::
            where('description','LIKE','%'.$request['search'].'%')
            ->orWhere('address','LIKE','%'.$request['search'].'%')
            ->orWhere('name','LIKE','%'.$request['search'].'%')
            ->union($comments)
            ->get();
        }
        else{
            $results = Organization_profile::all();
        }

        return view('parent.index')->with('parent',$parent)
            ->with('org_profiles',$results)
            ->with('comment_type',$comment_type)
            ->with('affiliates',$affiliates)
            ->with('streams',Stream::lists('name','id')) 
            ->with('courses', Course::lists('name','id'))
            ->with('organizations',Organization_profile::lists('name','id'));
    }
}
