<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Comment;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->user_typeId == 1) {
            $unpublished_comment = Comment::where('published','0')->count();
            view()->share('unpublished_comment', $unpublished_comment);
                
            return $next($request);
        }
        else
            return redirect('adminLogin');
    }
}
