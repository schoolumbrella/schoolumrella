<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','HomeController@index');
Route::get('/register','HomeController@register');
Route::post('/register','HomeController@create');
Route::get('/login','HomeController@login');
Route::post('/login','HomeController@authenticate');

Route::resource('comment','CommentController');//resource route creates 8 different routs.
Route::post('subscribe','HomeController@subscribe');

Route::get('getcoursesbyaffiliateId','CourseController@getCourses');
Route::get('getCourses','StreamController@getCourses');
Route::get('getOrganizationsByCourseId','OrganizationController@getOrganizationsByCourseId');

Route::get('/adminLogin','AdminController@index');
Route::post('admin/login','AdminController@login');

Route::group(['middleware'=>'organization'], function(){
	Route::get('organization','OrganizationController@index');
	Route::get('organization/edit','OrganizationController@edit');
	Route::post('organization/edit','OrganizationController@update');
	Route::post('organization/addCourses','OrganizationController@addCourses');	
	Route::post('organization/comment','OrganizationController@addComment');	
});

Route::group(['middleware'=>'parent'], function(){
	Route::get('parent','ParentController@index');
	Route::post('parent','ParentController@search');	
});

Route::group(['middleware'=>'student'], function(){
	Route::get('student','StudentController@index');	
});

Route::group(['middleware'=>'teacher','prefix' =>'teacher'], function(){
	Route::get('','TeacherController@index');
	Route::get('/resourceAdd','TeacherController@create');
	Route::post('/resourceAdd','TeacherController@store');
	Route::get('/resourceEdit/{id}','TeacherController@edit');
	Route::post('/resourceEdit/{id}','TeacherController@update');
	Route::get('/resourceDel/{id}','TeacherController@destroy');	
});

Route::group(['middleware'=>'admin','prefix' => 'myAdmin'], function(){
	Route::get('','AdminController@home');
	Route::get('edit','AdminController@edit');
	Route::get('affiliates','AffiliateController@index');
	Route::get('affiliates/add','AffiliateController@create');
	Route::post('affiliates/add','AffiliateController@store');
	Route::get('affiliates/edit/{id}','AffiliateController@edit');
	Route::post('affiliates/edit/{id}','AffiliateController@update');
	Route::get('affiliates/del/{id}','AffiliateController@destroy');
	
	Route::get('levels','LevelController@index');
	Route::get('levels/add','LevelController@create');
	Route::post('levels/add','LevelController@store');
	Route::get('levels/edit/{id}','LevelController@edit');
	Route::post('levels/edit/{id}','LevelController@update');
	Route::get('levels/del/{id}','LevelController@destroy');

	Route::get('streams','StreamController@index');
	Route::get('streams/add','StreamController@create');
	Route::post('streams/add','StreamController@store');
	Route::get('streams/edit/{id}','StreamController@edit');
	Route::post('streams/edit/{id}','StreamController@update');
	Route::get('streams/del/{id}','StreamController@destroy');

	Route::get('courses','CourseController@index');
	Route::get('courses/add','CourseController@create');
	Route::post('courses/add','CourseController@store');
	Route::get('courses/edit/{id}','CourseController@edit');
	Route::post('courses/edit/{id}','CourseController@update');
	Route::get('courses/del/{id}','CourseController@destroy');

	Route::get('resourceTypes','ResourceTypeController@index');
	Route::get('resourceTypes/add','ResourceTypeController@create');
	Route::post('resourceTypes/add','ResourceTypeController@store');
	Route::get('resourceTypes/edit/{id}','ResourceTypeController@edit');
	Route::post('resourceTypes/edit/{id}','ResourceTypeController@update');
	Route::get('resourceTypes/del/{id}','ResourceTypeController@destroy');

	Route::get('comments','AdminController@comment');
	Route::get('comment/edit/{id}','AdminController@editComment');
	Route::post('comment/edit/{id}','AdminController@updateComment');
	Route::get('comment/delete/{id}','AdminController@deleteComment');

	Route::get('subscribers','AdminController@subscriber');
	Route::post('subscribers','AdminController@message');
});

Route::controllers([
		'auth' => 'Auth\AuthController',
		]);
