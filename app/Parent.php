<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parent extends Model
{
    protected $table = 'parent_profiles';
    
    public function comments(){
    	return $this->hasMany('App\Comment','userId');
    }
}
