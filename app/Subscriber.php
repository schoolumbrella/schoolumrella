<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public static $rules = array('email' => 'required|unique:subscribers');
    public $fillable = array('email','ip');
}
// static data can be accessed without creating any Object
// $fillable is builtin member variable but not mendatory