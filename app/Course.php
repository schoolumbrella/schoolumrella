<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Organization_profile;
use App\Organization_course;

class Course extends Model
{
    public function affiliate(){
    	return $this->belongsTo('App\Affiliate','affiliateId');
    }
    public function stream(){
    	return $this->belongsTo('App\Stream','streamId');
    }
    public function level(){
    	return $this->belongsTo('App\Level','levelId');
    }
    public function organization_courses(){
    	return $this->hasMany('App\Organization_course','courseId');
    }
    public function organizations(){
    	//return $this->hasMany('App\Organization_course','organizationID');
    	$orgs = Organization_course::where('courseId',$this->id)->groupBy('organizationID')->get();
    	$organizations = [];

    	foreach ($orgs as $org) {
    		$organizations[] = Organization_profile::find($org->organizationId);
    	}
    	return $organizations;
    }
}
