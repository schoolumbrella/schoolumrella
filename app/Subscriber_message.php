<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber_message extends Model
{
    public static $rules = array('message' => 'required', 'userId' => 'required|exists:users,id');
    public $fillable = array('subject','message','userId');
}
