<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public $timestamps = false;

    public function resource_files(){
    	return $this->hasMany('App\Resource_file','resourceId');
    }

    public function resource_courses(){
    	return $this->hasMany('App\Resource_course','resourceId');
    }
}
