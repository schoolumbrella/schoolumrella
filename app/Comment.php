<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $rules = array(
    	'comment' => 'required',
    	);
    public function user(){
    	return $this->belongsTo('App\User','userId');
    }
    public function comment_type(){
    	return $this->belongsTo('App\Comment_type','comment_typeId');
    }
    public function commentRoot(){
    	if($this->comment_type->id == 1){
    		return User::find($this->commentRoot);
    	}
    	elseif ($this->comment_type->id == 2) {
    		return Resource::find($this->commentRoot);
    	}
    	else{
    		return Comment::find($this->commentRoot);
    	}
    }
    public function comments(){
        return $this->hasMany('App\Comment','commentRoot')->where('comment_typeId',3)->get();
    }
}
