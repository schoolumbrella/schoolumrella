<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization_profile extends Model
{
   public function courses(){
   	return $this->belongsToMany('App\Course', 'organization_courses', 'organizationId', 'courseId');
   }
   public function user(){
   	return $this->belongsTo('App\User','userId');
   }
}
