function changeUserType(){
	document.getElementById('organizationInfo').style.display = (document.getElementById('user_typeId').value == 5)? 'block':'none';
	document.getElementById('extraInfo').style.display = (document.getElementById('user_typeId').value == 5)? 'none' : 'block';

}

$('#commentModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var myid = button.data('myid') // Extract info from data-* attributes
  var modal = $(this)
  modal.find('.modal-body #organizationId').val(myid)
})

function affiliateChange() {
	debugger;
	// document.getElementById('courseId').innerHTML = 
	
	var xml = new XMLHttpRequest();
	xml.onreadystatechange = function() {
		if(xml.readyState == 4 && xml.status == 200) {
			var myCourses = JSON.parse(xml.responseText);
			var options = null;
			myCourses.forEach(function(ls,i){
				options += "<option value='"+ ls.id +"'>"+ ls.name +"</option>"; 
			});
			document.getElementById('courseId').innerHTML = options;
		}
	};
	xml.open("GET", window.location.origin + "/getcoursesbyaffiliateId?id="+document.getElementById('affiliateId').value, true);
	xml.send();

	///
	// posts = $.ajax({
	//     type: 'GET',
	//     url: "http://localhost/schoolumrella/public/getcoursesbyaffiliateId?id="+document.getElementById('affiliateId').value,
	//     async: false,
	//     dataType: 'json',
	//     data: { action : 'getHotelsList' },
	//     done: function(results) {
	//     	debugger;
	//         // uhm, maybe I don't even need this?
	//         JSON.parse(results);
	//         return results;
	//     },
	//     fail: function( jqXHR, textStatus, errorThrown ) {
	//     	debugger;
	//         console.log( 'Could not get posts, server response: ' + textStatus + ': ' + errorThrown );
	//     }
	// }).responseJSON;
	
}
function getCourses(){
	var xml = new XMLHttpRequest();
	xml.onreadystatechange = function() {
		if(xml.readyState == 4 && xml.status == 200) {
			var res = JSON.parse(xml.responseText);
			var myCourses = res.courses;
			var options = null;
			var orgsOptions = null;
			if(res.courses.length == 1){
				if(res.organizations.length > 0){
					res.organizations.forEach(function(ls,i){
						orgsOptions += "<option value='"+ ls.id +"'>"+ ls.name +"</option>";
					});
					//updateOrganization(orgsOptions,res.organizations);
					//document.getElementById('organizationId').innerHTML = orgsOptions;
				}
				//orgsOptions
			}
			
			myCourses.forEach(function(ls,i){
				options += "<option value='"+ ls.id +"'>"+ ls.name +"</option>"; 
			});
			document.getElementById('courseId').innerHTML = options;
			if(res.organizations == undefined){
				updateOrganization(orgsOptions,null);
			}
			else{
				updateOrganization(orgsOptions,res.organizations);
			}
			
			//document.getElementById('organizationId').innerHTML = orgsOptions;
		}
	};
	xml.open("GET", window.location.origin + "/getCourses?affiliateId="+document.getElementById('affiliateId').value+'&streamId='+document.getElementById('streamId').value, true);
	xml.send();

}
function getOrganizationsByCourseId(){
	var xml1 = new XMLHttpRequest(); // XMLHttpRequest() class is used for ajax call in  JavaScript
	xml1.onreadystatechange = function(){
		if(xml1.readyState == 4 && xml1.status == 200){
			 var myOrganizations = JSON.parse(xml1.responseText);
			 var options = null;
			 if(myOrganizations.length > 0){
			 	myOrganizations.forEach(function(myOrg, i){
			 		options += "<option value= '"+ myOrg.id + "'>"+ myOrg.name+" </option>";
			 	});
			 	updateOrganization(options,myOrganizations);
			 	//document.getElementById('organizationId').innerHTML = options;
			 }
			 else{
			 	updateOrganization(options,myOrganizations);
			 	//document.getElementById('organizationId').innerHTML = options;
			 }
		}
	}; //function() this function is assigned to 'onreadystatechange' even of xml1 object.
	xml1.open("GET", window.location.origin + "/getOrganizationsByCourseId?courseId="+document.getElementById('courseId').value, true);
	xml1.send();
}
function updateOrganization(orgsOptions,orgs){
	var org = orgs;
	document.getElementById('organizationId').innerHTML = orgsOptions;
	$('#orgTable > tbody  > tr').each(function(tr) {
		var myTr = this.id;
		var status = 0;
		if(org != undefined){
			org.forEach(function(l){
			if(l.id == myTr){
				status = 1;
			}
		});
		}
		if(status == 0){
			this.className = 'hidden';
		}
		else{
			this.className = 'block';
		}
	}); 
}
function selectOrg(){
	$('#orgTable > tbody  > tr').each(function(tr) {
		var status = 0;
		if(this.id == document.getElementById('organizationId').value){
			status = 1;
		}
		if(status == 0){
			this.className = 'hidden';
		}
		else{
			this.className = 'block';
		}
	}); 
}